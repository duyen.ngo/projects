import React, { Component } from 'react';
import { View, Image, StyleSheet, Text, TouchableHighlight, Alert } from 'react-native';



export default class example extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
    <View style={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }}>
        <Image style={styles.bgImg} source={{uri: 'http://dulichdongque.com/wp-content/uploads/2016/01/miet-vuon-vinh-kim.jpg'}}></Image>
        <View style={styles.contentwrap}>
            <View style={styles.proImgwrap}>
                <Image style={styles.proImg} source={{uri: 'http://cafefcdn.com/thumb_w/650/2017/cam-1487065611720.jpg'}}></Image>
            </View>
            <View style={styles.proInfoWrap}>
                <View style={styles.proInfo}>
                    <Text style={styles.proTitle}>Cam Đà Lạt</Text>
                    <Text style={styles.proPrice}>Gía: <Text style={{color: "red"}}>20.000</Text> VNĐ/Kg</Text>
                    <Text style={styles.proDes}>Cam nguyên chất, giá cả phải chăng</Text>
                    <View style={styles.buttonwrap}>
                        <View style={styles.line1}>
                            <Text style={{color: '#000', marginTop: 5}}>_________________</Text>
                        </View>
                        <TouchableHighlight
                            style= {styles.btn}
                            onPress={() => {
                            Alert.alert('You tapped the button!');
                            }}
                            >
                            <Text style= {styles.txt}> - </Text>
                        </TouchableHighlight>
                        <Text style={styles.proQuantity}>2</Text>
                        <TouchableHighlight
                            style= {styles.btn}
                            onPress={() => {
                            Alert.alert('You tapped the button!');
                            }}
                            >
                            <Text style={styles.txt}> + </Text>
                        </TouchableHighlight>
                        <View style={styles.line2}>
                            <Text style={{color: '#000', marginTop: 5}}>_________________</Text>
                        </View>
                    </View>
                    <Text style={styles.total}>Tổng tiền: <Text style={{color: "red"}}>40.000</Text> VNĐ </Text>
                </View>
            </View>
        </View>
        <View style={styles.btnwrap}>
            <TouchableHighlight
                style = {styles.addcartbtn}
                onPress={() => {
                Alert.alert('You tapped the button!');
                }}
                >
                <Text style={styles.addcarttxt}> Thêm Vào Gỉo </Text>
            </TouchableHighlight>
        </View>
    </View>
    
    );
  }
}

const styles = StyleSheet.create({
    bgImg: {
        width: '100%',
        height: '100%',
        opacity: 0.2,
    },
    proImgwrap: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
    },  

    contentwrap: {
        position: 'absolute',
        padding: 20,
    },

    proImg: {
        width: 200,
        height: 200,
        borderRadius: 15,
        borderWidth: 8,
        borderColor: '#ffffff',
    },

    proInfoWrap: {
        backgroundColor: 'transparent',
        marginTop: 20,
        borderRadius: 15,
        borderWidth: 8,
        borderColor: '#fff',
    },

    proInfo: {
        width: 350,
        height: 350,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        padding: 5
    },

    proTitle: {
        fontSize: 40,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'center'
    },

    proPrice: {
        fontSize: 20,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'right',
        paddingRight: 5,
    },

    proDes: {
        fontSize: 20,
        fontFamily: "Lobster-Regular",
        color: '#000',
        textAlign: 'left',
    },

    buttonwrap: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginTop: 30,
    },

    proQuantity: {
        fontSize: 40,
        fontFamily: "Lobster-Regular",
        color: '#000',
        marginTop: 0
    },

    total: {
        fontFamily: "Lobster-Regular",
        fontSize: 30,
        color: "#000",
        textAlign: "center"
    },

    btn: {
        width: 30, 
        height: 30, 
        backgroundColor: "#000", 
        borderRadius: 100, 
        marginTop: 5
    },
    
    txt: {
        color: '#fff', 
        textAlign: "center", 
        fontSize: 20
    },

    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 90,
    },

    addcartbtn: {
        position: "absolute",
        width: 250, 
        height: 50, 
        backgroundColor: "#FF6600", 
        borderRadius: 100, 
        marginTop: 5, 
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
    },

    addcarttxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 30,
        color: "#fff",
        textAlign: "center"
    }

  

})


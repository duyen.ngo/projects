import React, { Component } from 'react';
import { View, StyleSheet, Image, TextInput, Text, TouchableHighlight } from 'react-native';

export default class example2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <View style={{flex: 1, alignItems: "center"}}>
            <Image style={styles.backgroundImg} source={{uri: 'https://www.franchiseindia.com/uploads/content/ri/art/lodi-the-garden-restaurant-start-60f6b1bb83.jpg'}}></Image>
            <View style={styles.contentWrap}>
                <View style={styles.formWrap}>
                    <View style = {styles.titles}>
                        <TextInput style={styles.myinput}>Họ và Tên:          Ngô Thị Thanh Duyên</TextInput>
                    </View>
                    <View style = {styles.titles}>
                        <TextInput style={styles.myinput}>Số Điện Thoại:     0903524510</TextInput>
                    </View>
                    <View style = {styles.titles}>
                        <TextInput style={styles.myinput}>Địa Chỉ:     381 Lê Duẩn, Thanh Khê, Đà Nẵng</TextInput>
                    </View>
                </View>
                <View style={styles.btnwrap}>
                    <TouchableHighlight
                        style = {styles.confirmbtn}
                        onPress={() => {
                        Alert.alert('You tapped the button!');
                        }}
                        >
                        <Text style={styles.confirmtxt}> Xác Nhận và Đặt Hàng </Text>
                    </TouchableHighlight>
                </View>
            </View>
        </View>
    );
  }
}


const styles = StyleSheet.create({
    backgroundImg: {
        width: '100%',
        height: '100%',
        opacity: 0.2,
    },

    contentWrap: {
        flex: 1,
        position: "absolute",
        padding: 10,
    },

    formWrap: {
        width: 400,
        height: 600,
        borderRadius: 15,
        backgroundColor: 'transparent',
        borderWidth: 3,
        borderColor: '#000',
        marginTop: 0,
    },

    titles: {
        height: 50,
        borderWidth: 1,
        borderColor: "#000",
        borderRadius: 10,
        marginTop: 10,
        marginLeft: 5,
        marginRight: 5,
    },

    myinput: {
        fontFamily: "Lobster-Regular",
        fontSize: 20,
        color: "#000",
        marginLeft: 10,
    },

    mytext: {
        fontFamily: "Lobster-Regular",
        fontSize: 16,
        color: "#000",
        marginLeft: 10,
    },

    btnwrap: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 90,
    },

    confirmbtn: {
        position: "absolute",
        width: 350, 
        height: 50, 
        backgroundColor: "#FF6600", 
        borderRadius: 100, 
        marginTop: 5, 
        flex: 1,
        justifyContent: 'center',
        alignItems:'center',
    },

    confirmtxt: {
        fontFamily: "Lobster-Regular",
        fontSize: 25,
        color: "#fff",
        textAlign: "center"
    }



})  